#!/usr/bin/perl -w
#该步统计是将分开统计的一百条scaffold跑出来的Repeatmasker.gff文件的家族情况合并起来；是作为Repeats.Sub_family.pl的第二步
#文件输入顺序：统计出来的亚家族结果文件，输出文件名
use strict;
my $input=shift;
my $output=shift;
open (I,"<$input");
open (O,">$output");

my %repeats;
while (<I>){
    chomp;
    my $line=$_;
    if ($line=~m/\.\./){
	my $file=$line;
    }else{
	my @details=split/\s+/,$line;
	my $sub_family=$details[0];
	my $length=$details[1];
	my $percent_repeats=$details[2];
	my $percent_genome=$details[3];
	push (@{$repeats{length}{$sub_family}},$length);
#    $repeats{percent_rp}{$sub_family}=$percent_repeats;
	push (@{$repeats{percent_rp}{$sub_family}},$percent_repeats);
#    $repeats{percent_genome}{$sub_family}=$percent_genome;
	push (@{$repeats{percent_genome}{$sub_family}},$percent_genome);
    }
}

close I;
print O "types\trepeats length\t% of repeats\t% of genome\n";

my @subfamily_name=sort keys %{$repeats{length}};
foreach my $name (@subfamily_name){
    my @length=@{$repeats{length}{$name}};
    my $total_length=0;
    my $percent_rp=0;
    my $percent_genome=0;
    foreach my $length (@length){
	$total_length+=$length;
    }
    my @percent_rp=@{$repeats{percent_rp}{$name}};
    foreach my $percent1 (@percent_rp){
	$percent_rp+=$percent1;
    } 
    my @percent_genome=@{$repeats{percent_genome}{$name}};
    foreach my $percen2 (@percent_genome){
	$percent_genome+=$percen2;
    }
    print O "$name\t$total_length\t$percent_rp%\t$percent_genome%\n";
}
close O;
