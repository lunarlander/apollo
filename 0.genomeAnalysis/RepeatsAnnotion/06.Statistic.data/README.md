# 在该目录下进行数据统计，并为输出文件取好名字，以免造成混乱
# 依次统计的是： 
01.Repeats.overlapping.pl：统计各个软件的out文件转换成的gff文件中重复序列的信息，每个gff文件都需去冗余  
02.Repeats.Family.pl:统计除TRF以外的软件跑出来的gff文件中重复序列大家族的信息  
03.Repeats.Sub_family.pl：统计除TRF以外的软件跑出来的gff文件中重复序列亚家族的信息  
04.Repeats.Mask.fa.pl：将基因组fasta文件中为重复序列位点的片段覆盖掉，覆盖为小写碱基形式和n形式两个fasta文件  
05.Repeats.Diverge.pl：统计Repeatmasker和denovo跑出来的两个gff文件的相关重复序列的信息  