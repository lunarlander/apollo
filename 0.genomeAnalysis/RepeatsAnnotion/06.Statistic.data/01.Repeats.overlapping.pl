#!/usr/bin/perl -w
#统计各个软件跑出来的gff文件中重复序列的相关信息,需要去冗余
#各个文件输入的顺序需注意：输出文件名,TRF,repeatmasker,repeatproteinmask,denovo
use strict;
my $output=shift;
my @file=@ARGV;
open (O,">$output");
print O "Type\tRepeat length\t% of genome\n";

my $genome_dict=shift;
open (G,"<$genome_dict");
my $genome_size=0;
while (<G>){
    chomp;
    my $line_dict=$_;
    my @inf_dict=split/\s+/,$line_dict;
    my @dict=split/:/,$inf_dict[2];
    my $length=$dict[1];
    $genome_size+=$length;
}
close G;
#samtool后可得到一个基因组dict文件，用该dict文件来计算基因组大小
my @total_length;
my @percent;
for (my $i=0;$i<=@file-1;$i++){
    my $input=$file[$i];
    open (I,"<$input")||die ("$!\n");
    my %repeats;
    while (<I>){
        chomp;
        my $line=$_;
        my @information=split/\s+/,$line;
        my $Begin=$information[3];
        my $End=$information[4];
        my $seq_ID=$information[0];
        for (my $j=$Begin;$j<=$End;$j++){
            $repeats{$seq_ID}{$j}++;
        }
    }
    my @seq_name=keys %repeats;
    $total_length[$i]=0;
    foreach my $seq_name (@seq_name){
        my @begin_end=keys %{$repeats{$seq_name}};
        my @sort_begin_end=sort {$a<=>$b} @begin_end;
        my $length=scalar(@begin_end);
        $total_length[$i]+=$length;
    }
#    my $genome_size=479307600;
    $percent[$i]=$total_length[$i]/$genome_size*100; 
    #基因组大小可改动
    close I;
}
#print O "Repeatmasker\t$total_length[0]\t$percent[0]%\n";
print O "TRF\t$total_length[0]\t$percent[0]%\nRepeatmasker\t$total_length[1]\t$percent[1]%\nRepeatproteinmask_ABBlast\t$total_length[2]\t$percent[2]%\nRepeatmodeler\t$total_length[3]\t$percent[3]\n";
close O;
