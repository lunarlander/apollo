#!/usr/bin/perl -w
#统计个分歧度下重复序列家族占基因组的比例
#文件输入顺序：基因组dict文件，gff文件，输出文件名
use strict;
my $genome_dict=shift;
open (G,"<$genome_dict");
my $genome_size=0;
while (<G>){
    chomp;
    my $line_dict=$_;
    my @inf_dict=split/\s+/,$line_dict;
    my @dict=split/:/,$inf_dict[2];
    my $length=$dict[1];
    $genome_size+=$length;
}
print "$genome_size";
close G;

my $input=shift;
my $output=shift;
open (I,"<$input");
open (O,">$output");

my %repeats;
while (<I>){
    chomp;
    my $line=$_;
    my @details1=split/;/,$line;
    $details1[2]=~s/\?/_/;
    if ($details1[2]=~m#class/family=(\w+)/?#){ #匹配模式可改动
        my $family=$1;
        my $diverge;
        if ($details1[3]=~m#PercDiv=(\d+)#){
            $diverge=int($1);
        }else{
            print O "$line\n";
        }
        my @details2=split/\s+/,$line;
        my $begin=$details2[3];
        my $end=$details2[4];
        my $length=$end-$begin+1;
        $repeats{$family}{$diverge}+=$length;
    }else{
        print O "$line\n";
    }
}
#print O "family\tdiverge\ttotal length\t% of genome\n";
#my @Div=sort {$a<=>$b} keys %repeats;
my @family_name=keys %repeats;
foreach my $name_f (@family_name){
    my @Div=sort {$a<=>$b} keys %{$repeats{$name_f}};
    print O "Diverge\t$name_f\ttotal length\n";
    foreach my $Div (@Div){
        my $total_length=$repeats{$name_f}{$Div};
        my $percent=$total_length/$genome_size;
        print O "$Div\t$percent\t$total_length\n";
    }
}
close O;
close I;
