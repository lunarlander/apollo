#!/usr/bin/perl -w
#统计重复序列大家族的情况,，不用统计TRF软件的结果，并且不需要把所有软件跑出来的gff文件cat到一起再来跑
#文件输入顺序：基因组dict文件，输出文件名，各个软件的gff文件
use strict;
my $genome_dict=shift;
open (G,"<$genome_dict");
my $genome_size=0;
while (<G>){
    chomp;
    my $line_dict=$_;
    my @inf_dict=split/\s+/,$line_dict;
    my @dict=split/:/,$inf_dict[2];
    my $length=$dict[1];
    $genome_size+=$length;
}
print "$genome_size";
close G;

my $output=shift;
my @files=@ARGV;
open (O,">$output");
my @percent;
my @total_length;
for (my $i=0;$i<=@files-1;$i++){
    my $input=$files[$i];
    open (I,"<$input");
    print O "$input\n";
    my %family;
    my %repeats;
    while (<I>){
        chomp;
        my $line=$_;
        my @details=split/\s+/,$line;
        my $scaffold_name=$details[0];
        my @match=split/;/,$line;
#	$match[2]=~s/-/C/;
	$match[2]=~s/\?/_/;
        if ($match[2]=~m#Family=(\w+)/?#){
        #注意匹配模式，我是先将？转换为_来处理的；
#	if ($match[2]=~m#Family=[\w\?]+#){
            my $family_name=$1;
            for (my $j=$details[3];$j<=$details[4];$j++){
                $family{$family_name}{$scaffold_name}{$j}++;
                $repeats{$scaffold_name}{$j}++;
            }
#        }elsif ($match[2]=~m#class/family=((\w+)\?)/?#){
#	    my $family_name=$1;
#	    print "$family_name\n";
#            for (my $j=$details[3];$j<=$details[4];$j++){
#                $family{$family_name}{$scaffold_name}{$j}++;
#                $repeats{$scaffold_name}{$j}++;
#	    }
	}else{
	    print O "$line\n";
	}
    } 
    my @repeats=keys %repeats;
    foreach my $seq_name (@repeats){
        my @begin_end2=keys %{$repeats{$seq_name}};
        my $length2=scalar @begin_end2;
        $total_length[$i]+=$length2;
    }

    my @family_name=sort keys %family;
    foreach my $hash1 (@family_name){
        my @scaffold=keys %{$family{$hash1}};
        my $total_family=0;
        my $percent_family=0;
        my $percent_repeats=0;
        foreach (@scaffold){
            my @begin_end=keys %{$family{$hash1}{$_}};
            my $length=scalar(@begin_end);
            $total_family+=$length;
        }
        $percent_family=$total_family/$genome_size*100;
        $percent_repeats=$total_family/$total_length[$i];
	$hash1=~s/_/\?/; 
        #也可以不用将_转换回？
        print O "$hash1\t$total_family\t$percent_repeats\t$percent_family\n";
    }
    $percent[$i]=$total_length[$i]/$genome_size*100;
    print O "Total\t$total_length[$i]\t$percent[$i]\n";
    close I;
}
close O;

