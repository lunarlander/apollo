#!/usr/bin/perl -w
#dict文件为samtools跑出来的基因组dict文件，用于统计基因组大小
use strict;
my $genome_dict=shift;
open (G,"<$genome_dict");
my $genome_size=0;
while (<G>){
    chomp;
    my $line_dict=$_;
    my @inf_dict=split/\s+/,$line_dict;
    my @dict=split/:/,$inf_dict[2];
    my $length=$dict[1];
    $genome_size+=$length;
}
print "$genome_size";
close G;
