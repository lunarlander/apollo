#!usr/bin/perl -w
#Mask掉fasta文件中相应的重复序列位点，注意：生成两个mask文件，一个是将相应位点转换为小写，一个是将相应位点转换为n；
#文件输入顺序：转换成小写的mask输出文件名，转换成n的mask输出文件名，基因组fasta文件，需mask掉的gff文件
use strict;
use Bio::SeqIO;
my $output_L=shift;
my $output_N=shift;
my $input_genome=shift;
my $input_gff=shift;
open (I,"<$input_gff");
open (O1,">$output_L");
open (O2,">$output_N");

my %repeats;
while (<I>){
    chomp;
    my $line=$_;
    my @details=split/\s+/,$line;
    my $gff_name=$details[0];
    my $begin=$details[3];
    my $end=$details[4];
    my $length=$end-$begin+1;
    $repeats{$gff_name}{$begin}=$length;
}
close I;
#将每条重复序列的起始位置和长度信息存入hash中；
#注意位置信息的转换：substr是从0开始算的长度
my @gff_name=keys %repeats;
my $fa=Bio::SeqIO->new(-file=>$input_genome,-format=>'fasta');
while (my $seq_obj=$fa->next_seq){
    my $seq_name=$seq_obj->display_name;
    my $seq=$seq_obj->seq;
    my $string1=$seq;
    my $string2=$seq;
    if (exists $repeats{$seq_name}){
	my @position=keys %{$repeats{$seq_name}};
	foreach my $position (@position){
            #注意start是否需要-1;
	    my $start=$position-1;
	    substr($string1,$start,$repeats{$seq_name}{$position})=~s/(\w+)/\L$1/gi;
#	    print O1 ">$seq_name\n$string\n";
	    substr($string2,$start,$repeats{$seq_name}{$position})=~s/\w/n/gi;
#	    print O2 ">$seq_name\n$string\n";
	}
	print O1 ">$seq_name\n$string1\n";
	print O2 ">$seq_name\n$string2\n";
    }else{
	print O1 ">$seq_name\n$seq\n";
	print O2 ">$seq_name\n$seq\n";
    }
}
close O1;
close O2;
