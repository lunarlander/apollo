#!/usr/bin/perl -w
#计算重复序列亚家族的长度和比例
#文件输入顺序：基因组dict文件，输出文件，notmatch文件，各软件的gff文件
use strict;
my $genome_dict=shift;
open (G,"<$genome_dict");
my $genome_size=0;
while (<G>){
    chomp;
    my $line_dict=$_;
    my @inf_dict=split/\s+/,$line_dict;
    my @dict=split/:/,$inf_dict[2];
    my $length=$dict[1];
    $genome_size+=$length;
}
print "$genome_size";
close G;

my $output=shift;
my $output_notmatch=shift;
my @files=@ARGV;
open (O,">$output");
open (O1,">$output_notmatch");
#output为统计结果输出文件，output_notmatch为没有匹配上Family=(.+)模式的输出结果，便于调整匹配模式；
my @percent;
my @total_length;
for (my $i=0;$i<=@files-1;$i++){
    my $input=$files[$i];
    open (I,"<$input");
    print O "$input\n";
    my %family;
    my %repeats;
    while (<I>){
	chomp;
	my $line=$_;
	my @details=split/\s+/,$line;
	my $scaffold_name=$details[0];
	my @match=split/;/,$line;
#	if ($line=~m#Family=(\w+-?)(/|;)(.+)#){
	if ($match[2]=~m#Family=(.+)#){
	    my $family_name=$1;
	    for (my $j=$details[3];$j<=$details[4];$j++){
		$family{$family_name}{$scaffold_name}{$j}++;
	#	$repeats{$scaffold_name}{$j}++;	    
	    }
	}else{
	    print O1 "$line\n";
	}
	for (my $j=$details[3];$j<=$details[4];$j++){
	    $repeats{$scaffold_name}{$j}++;
	}
    }
    close O1;
    my @repeats=keys %repeats;
    foreach my $seq_name (@repeats){
        my @begin_end2=keys %{$repeats{$seq_name}};
        my $length2=scalar @begin_end2;
        $total_length[$i]+=$length2;
    }
	
    my @family_name=sort keys %family;
    foreach my $hash1 (@family_name){
	my @scaffold=keys %{$family{$hash1}};
	my $total_family=0;
	my $percent_family=0;
	my $percent_repeats=0;
	foreach (@scaffold){
	    my @begin_end=keys %{$family{$hash1}{$_}};
	    my $length=scalar(@begin_end);
	    $total_family+=$length;
	}
	$percent_family=$total_family/$genome_size*100;
	$percent_repeats=$total_family/$total_length[$i];
	print O "$hash1\t$total_family\t$percent_repeats\t$percent_family\n";
    }
    $percent[$i]=$total_length[$i]/$genome_size*100;
    print O "Total\t$total_length[$i]\t$percent[$i]\n";
    close I;
}
close O;



