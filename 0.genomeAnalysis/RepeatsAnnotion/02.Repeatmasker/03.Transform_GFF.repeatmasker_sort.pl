#!/usr/bin/perl -w
#用hash来处理
#将基因组的fasta文件里scaffold的顺序与软件跑出来的scaffold的顺序一致
#文件输入的顺序依次是基因组fasta文件，repeatmasker.out文件，gff文件，过滤掉的simple存放的文件
use strict;
use Bio::SeqIO;
my $input_genome=shift;
my $input=shift;
my $output_gff=shift;
my $output_simple=shift;
open (O,">$output_gff");
open (O2,">$output_simple");
my @genome_ID;

my $fa=Bio::SeqIO->new(-file=>$input_genome,-format=>'fasta');
while (my $seq_obj=$fa->next_seq){
    my $ID=$seq_obj->id;
    my $seq_name=$seq_obj->display_name;
    push (@genome_ID,$seq_name);
}
print "done\n";
#将基因组fasta文件中的scaffold名字取出来存入数组，以便之后比较

open (I,"<$input");
my $ID=0;
my %hash;
while (<I>){
    chomp;
    my $line=$_;
#    $line=~s/C/-/;
    $line=~s/^\s*//;
    if ($line=~m/Simple/){
        print O2 "$line\n";
        next;
#将属于simple家族的重复序列去掉，只留下转座子的
    }
    my @array1=split/\s+/,$line;
    $ID++;
    $hash{$array1[4]}{$ID}=$line;
}
close O2;
#创建hash的hash，使每个scaffold名字为第二层hash的名字，ID为第二层hash的键
#比较第一层hash的键与基因组数组中两个字符串是否相等
my $number=0;
my @keys_ID=keys %hash;
foreach my $ID_compare (@genome_ID){
    my @seq_ID=@keys_ID;
    my $remaining=@seq_ID;
    print "Remaining:$remaining\t";
    my $place=-1;
    foreach my $seq_ID (@seq_ID){
        $place++;
        if ("$ID_compare"eq"$seq_ID"){
            my @ID_scaffold=keys %{$hash{$seq_ID}};
            foreach my $ID_scaffold (@ID_scaffold){
                my @array=split/\s+/,$hash{$seq_ID}{$ID_scaffold};
		$array[4]=~/([a-zA-Z]+[0-9]+)/;
		my $scaffold=$1;
                $number++;
                my $len=length($number);
                my $i=8-$len;
                my $zero="0"x$i;
                my $ID="$zero"."$number";
                if ("$array[8]" eq "C"){
		    $array[8]=~s/C/-/;
		    print O "$scaffold\tRepeatmasker\tRepeat\t$array[5]\t$array[6]\t$array[0]\t$array[8]\t.\tID=TErm$ID;Target \"Motif:$array[9]\" $array[13] $array[12];Family=$array[10];PercDiv=$array[1];PercDel=$array[2];PercIns=$array[3]\n";
                }else{
		    print O "$scaffold\tRepeatmasker\tRepeat\t$array[5]\t$array[6]\t$array[0]\t$array[8]\t.\tID=TErm$ID;Target \"Motif:$array[9]\" $array[11] $array[12];Family=$array[10];PercDiv=$array[1];PercDel=$array[2];PercIns=$array[3]\n";
                }
            }
#将匹配到的out文件中的scaffold移除，这样下次匹配时便可以直接匹配其他scaffold名字，速度提高
            my @remove=splice @keys_ID,$place,1;
            print "Remove:$remove[0]\t";
            last;
        }
    }
    print "The place of match:$place\n";
}
close O;

