#!/usr/bin/perl -w
#生成将所有.out文件转换为.gff文件的sh文件的脚本
use strict;
my $output_sh='02.repeatmasker.transform.sh';
open (O,"$output_sh");
my $scaffold_dir='scaffold_100.dir';
my @inputs=<$scaffold_dir/*>;
my $perl5_path="PERL5LIB=$PERL5LIB:/share/work/user125/perl5/lib/perl5";
my $transformGFF_path="../02.Repeatmasker/02.Transform_GFF.repeatmasker_sort.pl";
foreach my $dir (@inputs){
    my $input_genome="$dir/$dir.fa";
    my $input_out="$dir/$dir.out";
    my $output_gff="$dir/$dir.out.gff";
    my $output_simple="$dir/$dir.simplt.gff";
    print O "export $perl5_path;perl $transformGFF_path $input_genome $input_out $output_gff $output_simple\n";
}
close O;
