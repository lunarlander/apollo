# Repeatmasker软件可以用来查找和屏蔽基因序列中的重复和低复杂度的序列。
# 1.Repeatmakser的安装： 
Repeatmakser相关信息请进入：http://www.repeatmasker.org/ 
## (1)Repeatmasker软件的安装： 
### a.进入网址：http://www.repeatmasker.org/RMDownload.html 
### b.下载网址(下载最新的linux64版本的)：wget -r http://www.repeatmasker.org/RepeatMasker-open-4-0-6.tar.gz  
### c.解压软件包，步骤如下：  
-gunzip repeatmaskerlibraries-*.tar.gz 
-tar xvf repeatmaskerlibraries-*.tar 
-rm repeatmaskerlibraries-*.tar  
### d.RMBlast and ABBlast搜索引擎的安装： 
RMBlast: 
进入网址：http://www.repeatmasker.org/RMBlast.html  
下载：wget -r ftp://ftp.ncbi.nlm.nih.gov/blast/executables/rmblast/2.2.28和wget -r ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.2.28/  
解压并安装，步骤如下：  
-tar zxvf ncbi-blast-2.2.28+-x64-linux.tar.gz  
-tar zxvf rmblast-2.2.28-x64-linux.tar.gz  
-cp -R rmblast-2.2.28/* ncbi-blast-2.2.28+/  
-rm -rf rmblast-2.2.28  
-mv ncbi-blast-2.2.28+ rmblast-2.2.28  
-ABBlast:  
进入网址：http://blast.advbiocomp.com/  
用川大邮箱申请下载ABBlast的账号，第二天便可收到ABBlast下载软件包，然后下载解压便可  
### e.安装，步骤如下： 
-cd /usr/local/RepeatMasker（进入你的Repeatmasker文件夹下）；  
-emacs RepeatMakser(进入Repeatmasker软件，将其第一行perl的路径改为自己服务器上perl的路径，我们实验室为usr/bin/perl，然后保存退出；  
-perl ./configure（找到configure，运行它，并按它的提示进行安装，第一步为perl的绝对路径：/usr/bin/perl;第二步为Repeatmasker的绝对路径;第三步为TRF的绝对路径;第四步为选择搜索引擎，可一次性安装两个RMBlast,ABBlast，最后选择5(done)，提示congrats说明安装成功）；  
## (2)Repbase的安装： 
### a.进入地址：http://www.girinst.org/repbase/，用川大邮箱申请一个非盈利账号，第二天即可收到Repbase的下载软件包 
### b.下载并解压Repbase：tar zxvf 
### c.将Repeatmasker文件夹里的Library文件夹与Repbase解压后的Library文件夹合并，重点是将RepeatMaskerLib.embl更新为最新版本 
# 2.Repeatmasker参数设置： 
## 纲要：RepeatMasker [-options] <seqfiles(s) in fasta format> 
## 参数：./Repeatmasker便可查看相关参数 
## 重要参数： -nolow，-no_is，-norna，-parallel，-species(-species "物种名")，-gff，-engine(选择一个引擎)，-lib(在repeatmodeler之后需要用到这个参数) 
## 注意species的选择：可进入http://www.girinst.org/repbase/update/browse.php查询相关物种 
# 3.Repeatmasker输出文件格式： 
## (1)*.out:被比对上重复序列的说明文件 
### 文件格式： 
   SW   perc perc perc  query                   position in query              matching         repeat                 position in repeat
score   div. del. ins.  sequence                begin    end          (left)   repeat           class/family       begin   end    (left)     ID

  640   14.6  0.6  0.0  scaffold100015_cov36          42      212      (476) + (TTA)n           Simple_repeat            2    173     (0)     1
  340   27.4  1.7  1.7  scaffold100084_cov50         347      461        (8) C ATCopia69_I-int  LTR/Copia            (247)   4023    3909     6
### 文件信息说明： 
-第一列：比对上的Smith-Waterman 分值  
-第二列：比上区间与共有序列相比的分化率即diverge  
-第三列：在查询序列中的碱基缺失的百分率（删除碱基）  
-第四列：在查询序列中的碱基缺失的百分率（插入碱基）  
-第五列：查询序列的名称  
-第六列：比对上区间在查询序列中的起始位置  
-第七列：比对上区间在查询序列中的终止位置  
-第八列：表示里查询序列3'末端还有多少碱基  
-第九列：表示被比对上的重复序列在查询序列中是处于正链还是负链，+表示正链，C表示负链  
-第十列：比对上的Repbase库中重复序列的名称  
-第十一列：重复序列所属的家族  
-第十二至十四列：分别为比上区间在Repbase库中重复序列的起始位置，终止位置以及离3'末端的碱基数目，但是若第九列为C，则这三列数目倒过来，总是"()"内的数字代表的是(left)  
-第十五列：序列ID  
## (2)*.masked:重复序列被屏蔽之后的文件 
## (3)*.tbl:重复序列各种信息的表格 
## (4)*.cat此文件内容同 *.out 
# 4.将基因组fasta文件转换为100条scaffold一个文件，然后生成.sh文件，提交任务: 
## 生成100条scaffold的脚本：$apollo/0.genomeAnalysis/RepeatsAnnotion/02.Repeatmasker/Cut_scaffold_100.fa.2.pl 
## 生成.sh文件的脚本：$apollo/0.genomeAnalysis/RepeatsAnnotion/02.Repeatmasker/build_sh.pl 
## 命令可写为：export PERL5LIB=$PERL5LIB:/share/work/user125/perl5/lib/perl5;/share/work/user125/software/RepeatMasker/RepeatMasker/RepeatMasker -nolow -no_is -norna -parallel 2 -species "All" -gff scaffold.1/scaffold.1.fa 
# 5.将所有*.out文件转换为*.gff文件并cat所有*.gff文件(注意转换gff时需要将属于重复序列simple家族的去掉)： 
## 转换脚本：$apollo/0.genomeAnalysis/RepeatsAnnotion/02.Repeatmasker/Transform_repeatmasker.GFF.sort.pl 
## gff格式：scaffold18_cov83 Repeatmasker Repeat 7 146 487 + . ID=TErm00000001;Target "Motif:Gypsy21-PTR_LTR" 147 288;Family=LTR/Gypsy;PercDiv=29.3;PercDel=1.4;PercIns=0.0 