#!usr/bin/perl -w
#将分scaffold跑完的已转换的gff文件全部合并到一起
#注意$input，可改为自己目录下文件的名字
#output的赋值给改动
use strict;
my $output="02.repeatmasker.all.gff";
open (O,">$output");
my $scaffold_dir="scaffold_100.dir";
my @files=<$scaffold_dir/*>;
for (my $i=1;$i<=@files-1;$i++){
    my $input="scaffold.$i/scaffold.$i.out.gff";
    open (I,"<$input");
    while (<I>){
	chomp;
	my $line=$_;
	print O "$line\n";
    }
    close I;
}
close O;
