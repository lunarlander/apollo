# Repeatmodeler用于Denovo预测转座子 
# 1.Repeatmodeler的安装： 
## 进入网页：http://www.repeatmasker.org/RepeatModeler.html 
## 下载网址(下载最新版本)：http://www.repeatmasker.org/RepeatModeler-open-1-0-8.tar.gz 
## 下载并解压： 
-gunzip RepeatModeler-open-1-*.tar.gz  
-tar xvf RepeatModeler-open-1-*.tar  
## 安装：运行./configure，按照提示进行 
注意：安装时要注意安装RepeatScout-1和RECON-1，查看install或readme文件，进行安装；另外，在输入trf路径之前，你应该重新安装一次trf，并把软件名字改为trf，这样才能安装成功  
# 2.Repeatmodeler参数设置： 
## (1)建数据库的命令：[RepeatModelerPath]/BuildDatabase -name <新库的名字>(如elephant) (-engine ncbi) 基因组.fa文件(如scaffold_efficient.fa) (注意:engine这个参数可以不用设置) 
## (2)运行Repeatmodeler建立library的命令：[RepeatModelerPath]/RepeatModeler -database <新库的名字> (跑完这一步之后会出现一个RM_<PID>.<DATE>格式的文件夹，例如："RM_5098.MonMar141305172005"，在该文件夹下找到consensi.fa.classified文件，该文件可为Repeatmasker所用) 
## (3)运行repeatmasker寻找重复序列的命令：[RepeatMaskerPath]/RepeatMasker -lib consensi.fa.classified mySequence.fa(参考repeatmasker) 
