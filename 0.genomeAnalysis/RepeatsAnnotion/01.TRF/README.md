# 重复序列包括串联重复和分散重复序列，TRF是用来搜寻DNA序列中的串联重复序列（相临的重复两次或者多次特定核酸序列模式的重复序列）。 
# 1.TRF下载： 
进入网址：https://tandem.bu.edu/trf/trf.download.html  
下载最新版本（目前为 Version 4.07b）：wget -r https://tandem.bu.edu/trf/trf407b.linux.download.html 或者 https://tandem.bu.edu/trf/trf407b.linux64.download.html（64位的）  
还可进入网址来查看更多内容（参数，输出结果的信息说明）：https://tandem.bu.edu/trf/trf.unix.help.html  
# 2.TRF参数设置： 
默认参数为：2 7 7 80 10 50 2000 -d -h  
其余参数直接./trf便可查看有哪些参数  
# 3.将基因组fasta文件分为100条scaffold一个文件，放入相应的文件夹下，然后再运行TRF软件，最后将结果整合到一起。  
# 4.TRF软件跑出来的文件格式： 
程序运行完，会在目录下生成以下几个文件：  
## (1).*.dat:-d 参数产生的屏蔽的的串联重复序列信息文件。 
### 软件信息 
Sequence: Scaffold1  


Parameters: 2 7 7 80 10 50 500  


1131 1171 18 2.2 19 95 4 75 48 14 9 26 1.75 TAAGCTATACACATTAAAG TAAGCTATACACATTAAATAAGCTATACACATTAAAGTAAG  
2219 2279 29 2.1 29 100 0 122 24 3 13 59 1.49 TATGACAATTTATTTTTTTATGTTGGTAT TATGACAATTTATTTTTTTATGTTGGTATTATGACAATTTATTTTTTTATGTTGGTATTAT  
2200 2342 77 1.9 77 94 2 252 30 6 10 53 1.60 TATTCCTGATATTAAATAATATGACAATTTATTTTTTTATGTTGGTATTATCACAATTTAATTTTTTATGTTGGTAT TATTCCTGATATTAAATAATATGACAATTTATTTTTTTATGTTGGTATTATGACAATTTATTTTTTTATGTTGGTATTATTCCTGATATTAAATAATATGACAATTTATTTTTTTATGTTGGTATTATCACAATTAGATTTTT  


### 以此为重复单元； 
#### 信息说明：例如：1131 1171 18 2.2 19 95 4 75 48 14 9 26 1.75 TAAGCTATACACATTAAAG TAAGCTATACACATTAAATAAGCTATACACATTAAAGTAAG 
-1131 1171为串联重复在输入序列里的起始、终止位置  
-18为比对文件中串联重复单元的大小  
-2.2为重复次数  
-19为库里的重复序列得大小  
-95为与邻近的重复匹配的百分比  
-4为与邻近的重复匹配的插入删除百分比  
-75为匹配的分值  
-48,14，9,26分别为ATCG四种碱基在重复单元中所占的比例  
-1.75为重复序列包含的信息量，即熵值  
## (2).*.mask -m 参数产生的串联重复序列被屏蔽为Ｎ的序列文件 
## (3).*.html 记录串联重复序列信息的文件 
## (4).*.html 记录串联重复序列信息的文件 
# 5.将*.dat文件转换为gff格式的文件 
## (1).gff格式： 
### 进入该网址查询：http://asia.ensembl.org/info/website/upload/gff.html?redirect=no； 
### 格式说明:  
-a.序列名字  
-b.文件来源：可写用来跑的软件  
-c.特征：属于什么，例如这段信息为重复序列就写为repeats，为基因就可写为gene...  
-d.起始位置  
-e.终止位置  
-f.分值：该特征的分值  
-h.链：正链or负链  
-i.frame - One of '0', '1' or '2'. '0' indicates that the first base of the feature is the first base of a codon, '1' that the second base is the first base of a codon, and so on.  
-j.属性：用分号隔开每个属性，这些附加信息可包括序列ID，缺失插入比例，重复序列单元等  
## (2).转换脚本路径：$apollo/0.genomeAnalysis/RepeatsAnnotion/01.TRF/Trasform_GFF.pl; 
## (3).打断基因组scaffold的脚本并生成.sh文件：$apollo/0.genomeAnalysis/RepeatsAnnotion/01.TRF/Cut_scaffold_100.fa.2.pl 