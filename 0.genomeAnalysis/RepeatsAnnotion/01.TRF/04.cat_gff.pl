#!/usr/bin/perl -w
#将分scaffold跑完的已转换的gff文件全部合并到一起
#output为合并后的gff文件的名字
#$scaffold_dir为存放100条scaffold一个文件夹的总目录
use strict;
my $output=shift;
open (O,">$output");
my $scaffold_dir="../01.TRF/scaffold_100.dir";
my @files=<$scaffold_dir/*>;
for (my $i=1;$i<=@files-1;$i++){
    my $input="scaffold.$i/scaffold.$i.out.gff";
    open (I,"<$input");
    while (<I>){
	chomp;
	my $line=$_;
	print O "$line\n";
    }
    close I;
}
close O;
