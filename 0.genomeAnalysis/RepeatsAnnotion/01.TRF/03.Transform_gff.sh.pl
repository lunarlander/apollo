#!/usr/bin/perl -w
#生成将所有.out文件转换为.gff文件的sh文件的脚本
#inputs为存放100条scaffold一个文件夹的目录下所有的scaffold文件夹，给改为自己的路径；
#script_gff可改动，改为自己的转换gff文件的脚本路径
#注意：提交任务时需要在$scaffold_dir的上一层目录下提交
use strict;
my $output_sh="01.trf.transform_gff.sh";
open (O,"$output_sh");
my $scaffold_dir='../01.TRF/scaffold_100.dir';
my @inputs=<$scaffold_dir/*>;
my $perl5_path="PERL5LIN=$PERL5LIB:/share/work/user125/perl5/lib/perl5";
my $script_gff="../01.TRF/02.Transform_gff.pl";
foreach my $dir (@inputs){
    my $files="$dir/$dir.out";
    my $output="$dir/$dir.out.gff";
    print O "export $perl5_path;perl $script_gff $files $output\n";
}
close O;
