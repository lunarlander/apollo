# 提前准备  
1.将数据链接到自己的目录下，并将相同reads合并在一起 #将同样是500.1的reads合并在一起 zcat 500bp_1_1.fq.gz  500bp_2_1.fq.gz | gzip - >500bp.1.fq.gz 注：(DSC318.*.fq.gz是重测序得到的系列，reads大小为500bp)  
 #very important! 数据的命名规则：
 插入片段小于1000bp的单位为bp,命名规则为为：字母（物种名）.插入片段大小bp.1/2.fq ；如：Populus_ilicifolia.500bp.1.fq ，Populus_ilicifolia.500bp.2.fq
 插入片段大于1000bp的单位为kbp,命名规则为：字母（物种名）.插入片段大小kbp.1/2.fq ；如：Populus_ilicifolia.5kbp.1.fq ，Populus_ilicifolia.5kbp.2.fq
2.QualityControlAndMergeReads.pl脚本路径:  
3.lighter软件下载：git clone https://github.com/mourisl/Lighter.git 安装：make  
4.fastuniq软件下载：wget "http://sourceforge.net/projects/fastuniq/files/latest/download" 解压缩包：tar zxvf FastUniq-1.1.tar.gz 安装：cd source; make  

# reads的过滤  
1.脚本过滤  
**QualityControlAndMergeReads.pl scriptfilter.500bp Populus_ilicifolia.500bp.1.fq.gz Populus_ilicifolia.500bp.2.fq.gz**  
#(scriptfilter.500bp)为输出文件的前缀，输出文件为：scriptfilter.500bp.1.filter.fq.gz scriptfilter.500bp.2.filter.fq.gz  

2.使用lighter软件过滤短插入片段（所有小于2k）的reads  
**../Lighter-master/lighter -r scriptfilter.500bp.1.filter.fq.gz -r scriptfilter.500bp.2.filter.fq.gz -k 17 600000000 0.156 -t 20 -trim**  
# -r reads文件（fasta或fastaq格式），如果有很多文件，可以跟在很多-r的后面；-k k-mer length; 600000000 基因组大小粗略的估计值；0.156 α值，α=7/平均测序深度；-t 线程数；-trim 允许trim  

3.使用fastuniq过滤长插入片段（大于2k）的reads  
 注：3-1 fastuniq不支持压缩文件，使用时要先解压 zcat ***.fq.gz > ***.fq  
     3-2 需要提前生成一个列表文件,并将pair-end的一对reads写入文件中 如：emacs ip_10kbp.txt 写入../scriptfilter.10kbp.1.filter.fq ../scriptfilter.10kbp.2.filter.fq  
**/FastUniq/source/fastuniq -i ip_2kbp.txt -t q -o op_2kbp.1.fq -p op_2kbp.2.fq**  
# -i 列表文件；-t 输出文件的格式：q为fastq格式，f为fasta格式输出两个文件，p为fasta格式输出一个文件；-o 第一个输出文件 -p 第二个输出文件  
  
END  
