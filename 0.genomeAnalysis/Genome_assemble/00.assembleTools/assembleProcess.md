# 提前准备  
1.platanus软件路径： 
（cp）/share/work/user124/software/02.genome_assemble/platanus  
2.SOAPdenovo2-src-r240软件路径:  
（cp）/share/work/user124/software/02.genome_assemble/SOAPdenovo2-src-r240  

# 组装流程
 #1~3的sh文件可用"02.Platanus.sh.pl"来生成  
1. assemble（此命令基于Bruign图的算法来组装contig）  
**../platanus assemble -f scriptfilter.500bp.1.filter.cor.fq scriptfilter.500bp.2.filter.cor.fq scriptfilter.800bp.1.filter.cor.fq scriptfilter.800bp.2.filter.cor.fq -t 48 -m 480 -u 0.2 -o pilout** 
# -f 输入文件，这里要将所有短片段的reads都写入；-t 线程 -m 限制内存 -o 输出文件前缀  

2.scaffold（将paired reads比对到contig上，确定contig的顺序和方向，构建scaffolds） 
**../platanus scaffold -c pilout_contig.fa -b pilout_contigBubble.fa -IP1 Populus_ilicifolia.500bp.1.filter.cor.fq Populus_ilicifolia.500bp.2.filter.cor.fq -IP2 Populus_ilicifolia.800bp.1.filter.cor.fq Populus_ilicifolia.800bp.2.filter.cor.fq -OP3 op_2kbp.1_fastq op_2kbp.2_fastq -OP4 op_5kbp.1_fastq op_5kbp.2_fastq -OP5 op_10kbp.1_fastq op_10kbp.2_fastq -t 50 -o pilout**  
# -c 上一步组装出的连续contig序列；-b 上一步组装出的融合并删除bubble的序列；-IP1，-IP2……后面跟短插入片段的paired reads；-OP3，-OP4……后面跟长插入片段的paired reads；-t线程数；-o 输出文件前缀  

3.gap close(将paired reads比对到scaffold上，将reads定位到gap上，关闭一些gap)  
**../platanus gap_close -c pilout_scaffold.fa -IP1 Populus_ilicifolia.500bp.1.filter.cor.fq Populus_ilicifolia.500bp.2.filter.cor.fq -IP2 Populus_ilicifolia.800bp.1.filter.cor.fq Populus_ilicifolia.800bp.2.filter.cor.fq -OP3 op_2kbp.1_fastq op_2kbp.2_fastq -OP4 op_5kbp.1_fastq op_5kbp.2_fastq -OP5 op_10kbp.1_fastq op_10kbp.2_fastq -t 50 -o 03.pil_gapclose**  
#-c 上一步组装出的scaffold序列；其他参数含义同上，最后得到pilout.gapclosed.fa文件  

4.用SOAPdenovo2进行gap close  
 #在运行soap软件时，首先要写配置文件，将序列信息都写入config中："03.SOAPbuildconfig.pl"  
**../GapCloser -a 03.platanusGapClose_gapClosed.fa -b 04.soapGapClose.config -o 04.soapGapClose.fa -t 50 -l 100  
#-a 上一步platanus gapclose得到的文件；-b config文件； -o 输出文件； -t 线程；-l 最小的reads length**  

5. 用脚本过滤去掉N，有效长度小于200bp的序列："04.assembleFilter.pl"  
**perl 05.filter_200.pl 04.soapGapClose.fa（输入文件） 输出文件**  

# END
