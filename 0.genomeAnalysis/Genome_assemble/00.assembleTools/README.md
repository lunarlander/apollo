#此目录下的脚本是在基因组组装过程中需要用到的:  
01.QualityControlAndMergeReads.pl 脚本用于reads的第一步过滤;  
02.Platanus.sh.pl 脚本用于Platanus组装时生成.sh文件;  
03.SOAPbuildconfig.pl 脚本用于SOAP生成配置文件;  
04.assembleFilter.pl 脚本用于组装gap close 后的进一步过滤，去掉不包括N的有效长度小于200bp的序列;  
05.splitGenomeCallSNP.sh.pl 脚本用于call SNP时，分scaffold提交任务，可以同时分成每100个scaffold一个文件，并产生一个总的sh文件;  
06.mergeVcf.pl 脚本用于将分scaffold跑的vcf结果合并  

#readsFilterProcess.txt为reads过滤的具体流程和注意事项  

#assembleProcess.txt为组装的具体流程及注意事项  