#!/usr/bin/perl -w
#此脚本用于将基因组fasta文件分成每100条($eve)scaffold一个文件，并将输出call SNP的sh文件；
#使用时需要将$samtools，$genomefasta，$bam，$bcftools，$output换为自己的路径；可以调整$eve的大小；$o为输出的文件夹名以及文件名的前缀（数字）；
use strict;
use Bio::SeqIO;
my $inputfile=shift;
my $eve = 100;
my $sh = "$0.sh";
my $samtools = "/share/work/user124/software/02.genome_assemble/sametools/samtools/samtools";
my $genomefasta = "/share/work/user124/project/01.Populus_ilicifolia_genome/01.Assemble/03.platanusreads/Pil/04.filter200_soapGapclose.fa";
my $bam = "/share/work/user124/project/01.Populus_ilicifolia_genome/01.Assemble/05.callSNP/pil.realn.bam";
my $bcftools = "/share/work/user124/software/02.genome_assemble/sametools/bcftools/bcftools";
my $output = "/share/work/user124/project/01.Populus_ilicifolia_genome/01.Assemble/05.callSNP/test/vcf";
my $bedfile;
my $fastafile;
my $o =1;
my $num=1;
#$num记录读到了第几条序列；

open(S,">$0.sh") || die "$!\n";
my $fa = Bio::SeqIO->new(-file=>$inputfile, -format=>'fasta');
while(my $seq= $fa->next_seq){
    my $id = $seq->id;
    my $seq = $seq->seq;
    if ($num%100==1){
        `mkdir $o `;
        $fastafile = $o."/".$o.".fa";
        $bedfile = $o."/".$o.".bed";
        $o++;
        print S "$samtools mpileup -ug -t DP -t DP4 -l $bedfile -f $genomefasta $bam | $bcftools call -vmO z -o $output/$o.vcf.gz\n";
    }
    open (O1,">>$fastafile") || die "$!\n";
    print O1 ">$id\n$seq\n";
    close O1;
    open (O2,">>$bedfile") || die "$!\n";
    my $end = length($seq);
    print O2 "$id\t1\t$end\n";
    $num++;
}
