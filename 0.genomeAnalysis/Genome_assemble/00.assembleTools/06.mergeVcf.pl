#!/usr/bin/perl -w
#此脚本用于合并vcf文件
#脚本路径 输出文件 输入文件
use strict;
my $op = shift;
my @file = @ARGV;
open (O,">$op") || die ("$!/n");
for (my $j=0;$j<@file;$j++){
    open (F,"zcat $file[$j]|") || die ("$!/n");
    if ($j==0){
        while (<F>){
            chomp($_);
            print O "$_\n";
        }
    }
    else{
        while (<F>){
            chomp($_);
            if (/^#/){
	next;
            }
            else{
	print O "$_\n";
	last;
            }
        }
        while(<F>){
            chomp($_);
            print O "$_\n";
        }
    }
}
