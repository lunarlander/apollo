基因组的组装包括:  
# 1.基因组的过滤  
   1.1按照质量值过滤所有的reads  
   1.2使用lighter软件过滤短插入片段（<2kbp）的reads  
   1.3使用fastuniq软件过滤长插入片段（>2kbp）的reads  
# 2.基因组的组装  
  ## 2.1用platanus进行组装  
        2.1.1 assemble --- 用Bruign算法组装contig  
        2.1.2 scaffold --- 将paired reads比对到contig上，确定contig的顺序和方向，构建出scaffold  
        2.1.3 gap_close --- 将paired reads比对到scaffold，将reads定位到gap上，关闭一些gap  
  ## 2.2用Soap软件进行gap_close  
  ## 2.3用脚本过滤不包括N的有效长度小于200bp的序列，得到最终的序列  
# 3.统计图表  
   1.原始数据量统计表（insert_size/average_reads_length/total_data/sequence_depth/）  
   2.reads过滤后数据量统计表（insert_size/average_reads_length/total_data/sequence_depth/）  
   3.K-mer深度频数分布图  
   4.K-mer分析统计表格（K-mer_num/peak_depth/genome_size/used_bases/X/）  
   5.基因组组装长度统计表  
   6.GC含量与测序深度关系分布图  
   7.相关物种的基因组GC含量分布图  
   8.测序深度分布图  