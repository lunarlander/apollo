#! /usr/bin/perl
use strict;
use warnings;
use Bio::SeqIO;
use List::Util;

my $file="standard_output.final.scaffolds.fasta";
my $dict="standard_output.final.scaffolds.fasta.dict";
my $grouplist="groups.lst";
my $outdir="groups";
my $groupNumber=50; # 随机将基因组分成多少等份

`mkdir $outdir` if(!-e $outdir);

my @chr;
open(I,"< $dict");
while (<I>) {
    next unless(/SN:(\S+)\s+LN:(\d+)/);
    my ($chr,$len)=($1,$2);
    $chr=~/(scaffold\d+)/;
    $chr=$1;
    push @chr,$chr;
}
close I;

my %group;
my %info;
@chr=List::Util::shuffle @chr;
my $len=int(scalar(@chr)/$groupNumber)+1;
my $count=0;
my $group_no=0;
open(O,"> $grouplist");
for(my $i=0;$i<@chr;$i++){
    if($count==0){
	$group_no++;
    }
    $count++;
    if($count>=$len){
	$count=0;
    }
    my $chr=$chr[$i];
    $group{$group_no}{$chr}=1;
    $info{$chr}=$group_no;
    print O "$chr\t$group_no\n";
}
close O;

my %fh;
foreach my $group(keys %group){
    `mkdir $outdir/$group` if(!-e "$outdir/$group");
    open($fh{$group},"> $outdir/$group/ref.fa");
}

my $fa=Bio::SeqIO->new(-file=>$file,-format=>'fasta');

while(my $seq=$fa->next_seq){
    my $id=$seq->id;
    my $seq=$seq->seq;
    $id=~/(scaffold\d+)\|size(\d+)/;
    my $name=$1;
    my $len=$2;
    my $group=$info{$name};
    $fh{$group}->print(">$name\n$seq\n");
}

foreach my $group(keys %fh){
    close $fh{$group};
}
