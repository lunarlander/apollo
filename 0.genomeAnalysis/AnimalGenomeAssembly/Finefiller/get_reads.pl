#! /usr/bin/env perl
use strict;
use warnings;
# use FileHandle;

my $outdir="groups";
my $dict="standard_output.final.scaffolds.fasta.dict";
my $grouplist="groups.lst";

die "Cannot find $outdir!\n" if(!-e "$outdir");

my $insert=shift;
my $id=shift;

die "Usage: $0 <insert size> <id>\n" if(!$insert || !$id);

print STDERR "$insert\t$id\n";

my %info;
open(I,"< $grouplist");
while(<I>){
    chomp;
    my @a=split(/\s+/);
    my ($chr,$group)=@a;
    $info{$chr}=$group;
}
close I;

my %fh;
open(I,"< $dict");
while (<I>) {
    next unless(/SN:(\S+)\s+LN:(\d+)/);
    my ($chr,$len)=($1,$2);
    $chr=~/(scaffold\d+)/;
    $chr=$1;
    my $group=$info{$chr};
    if(!-e "$outdir/$group"){
	die "Please run step2!\n$group\n";
    }
    if(exists $fh{$group}{fq1} && $fh{$group}{fq2}){
        next;
    }
    open($fh{$group}{fq1},"| gzip - > $outdir/$group/$id.1.fq.gz") or die "Cannot create $outdir/$group/$id.1.fq.gz";
    open($fh{$group}{fq2},"| gzip - > $outdir/$group/$id.2.fq.gz") or die "Cannot create $outdir/$group/$id.2.fq.gz";
}
close I;

my $num=0;
print  "start\n";
while (<>) {
    next if(/^\@/);
    my $line1=$_;
    my $line2=<>;
    chomp $line1;
    chomp $line2;
    my @a=split(/\s+/,$line1);
    my @b=split(/\s+/,$line2);
    my $flag_left=$a[5];
    my $flag_right=$b[5];
    if($flag_left eq $flag_right){
        next if($flag_left =~/^\d+M$/ || $flag_left eq "*");
    }
    my ($group1,$group2)=("NA","NA");
    if(!($a[1] & 4)){
        my $name_pre=$a[2];
        $name_pre=~/(scaffold\d+)\|size(\d+)/;
        $name_pre=$1;
        $group1=$info{$name_pre};
    }
    if(!($b[1] & 4)){
        my $name_pre=$b[2];
        $name_pre=~/(scaffold\d+)\|size(\d+)/;
        $name_pre=$1;
        $group2=$info{$name_pre};
    }
    $num++;
    # print STDERR "Proceeding...$num...\r";

    if($insert>=2000){
        $a[9]=~tr/ATCGatcg/TAGCtagc/;
        $a[9]=reverse($a[9]);
        $a[10]=reverse($a[10]);
    }
    else{
        $b[9]=~tr/ATCGatcg/TAGCtagc/;
        $b[9]=reverse($b[9]);
        $b[10]=reverse($b[10]);
    }

    if($group1 eq $group2){
        $fh{$group1}{fq1}->print("@"."$a[0]/1\n$a[9]\n+\n$a[10]\n");
        $fh{$group1}{fq2}->print("@"."$b[0]/2\n$b[9]\n+\n$b[10]\n");
    }
    else{
        if($group1 ne "NA"){
            $fh{$group1}{fq1}->print("@"."$a[0]/1\n$a[9]\n+\n$a[10]\n");
            $fh{$group1}{fq2}->print("@"."$b[0]/2\n$b[9]\n+\n$b[10]\n");
        }
        if($group2 ne "NA"){
            $fh{$group2}{fq1}->print("@"."$a[0]/1\n$a[9]\n+\n$a[10]\n");
            $fh{$group2}{fq2}->print("@"."$b[0]/2\n$b[9]\n+\n$b[10]\n");
        }
    }
}

foreach my $group(keys %fh){
    close $fh{$group}{fq1};
    close $fh{$group}{fq2};
}
