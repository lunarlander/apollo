#! /usr/bin/env perl
use strict;
use warnings;

my $genomefile="standard_output.final.scaffolds.fasta";
my $outdir="groups";

`mkdir $outdir` if(!-e "$outdir");
system("samtools dict $genomefile > $genomefile.dict");
system("/home/share/user/user101/software/bowtie2/bowtie2-2.2.6/bowtie2-build $genomefile $genomefile");
