#! /usr/bin/env perl
use strict;
use warnings;

my @fq1=<clean_reads/*.1.fq.gz>;
my $ref="standard_output.final.scaffolds.fasta";

open(O,"> $0.sh");
foreach my $fq1(@fq1){
    my $fq2=$fq1;
    $fq2=~s/1.fq.gz/2.fq.gz/;
    $fq1=~/\/(.*)\.1\.fq\.gz/;
    my $lib=$1;
    $lib=~/(\d+)bp/;
    my $insert=$1;
    print O "/home/share/user/user101/software/bowtie2/bowtie2-2.2.6/bowtie2 -p 16 --very-sensitive --no-sq -x $ref -1 $fq1 -2 $fq2 | perl get_reads.pl $insert $lib\n";
}
close O;
