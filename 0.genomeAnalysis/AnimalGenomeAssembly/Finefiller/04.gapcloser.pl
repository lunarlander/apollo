#! /usr/bin/env perl
use strict;
use warnings;

my $outdir="groups";
my $gapcloser="/home/share/user/user101/software/soapdenovo/GapCloser";

my @fa=<$outdir/*/*.fa>;

open(O,"> $0.sh");
foreach my $fa(@fa){
    $fa=~/^(.*)\/(\w+\.fa)/;
    my $dir=$1;
    $fa=$2;
    print "cp soap.prepare.pl $dir ; cd $dir;  perl soap.prepare.pl ; $gapcloser -a $fa -b soap.config -o $fa.GC -p 31 -t 16 ; cd - \n";
}
close O;
