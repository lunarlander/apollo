#!/usr/bin/perl -w
use strict;
#用此脚本来统计gff文件中的重复序列的含量，1：输入输出文件路径 2：输入基因组大小文件路径 3：输入gff文件
my ($outfile,$genomedict,@file)=@ARGV;
die "$0 Usage:<out file><genome dict><gff file>...\n" if(@ARGV<3);
my $mark=1000;#定义一个程序运行mark

print"Loading genomesize.....\n";
my $genomesize=0;
open(F1,"$genomedict");
while(my $line=<F1>){
    chomp $line;
    if($line=~/(LN\:)([0-9]+)/){
	$genomesize=$genomesize+$2;
    }
}
close F1;

open(O,">$outfile");
print O "Type\tRepeat Size(bp)\t%of genome\n";
foreach my $file(@file){
    print "Loading $file...\n";
    my $lengthnum=0;
    my %statishash;
    my $NUM;
    open(F,$file);
    while(<F>){
	chomp;
	my $line=$_;
	my @element=split /\t/,$line;
	my $scaffoldname=$element[0];	
	my $start=$element[3];
	my $end=$element[4];
	if(!$start || !$end){
	    print STDERR "HERE:\n$line\n";
	    die "$file\n";
	}
	for($start;$start<=$end;$start++){
	    $statishash{$scaffoldname}{$start}++;
	}#start
	$NUM++;
	if($NUM%$mark==0){print"$NUM\n";}
    }#while
    close F;

    print"stastic...\n";
    $NUM=0;
    my @scaffoldname=keys %statishash;
    foreach my $scaffoldID(@scaffoldname){
	my $templegth=0;
	my @ID=keys %{$statishash{$scaffoldID}};
	foreach my $id(@ID){
	    $templegth++;
	}#id
	$lengthnum=$lengthnum+$templegth;
	$NUM++;
	if($NUM%1000==0){print"$NUM\n";}
    }#scaffoldID
    my $per_of_genome=($lengthnum/$genomesize)*100;
    print O "$file\t$lengthnum\t$per_of_genome\n";    
}#file
close O;


