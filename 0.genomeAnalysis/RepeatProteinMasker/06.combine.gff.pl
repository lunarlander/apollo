#!/usr/bin/perl -w
#此脚本用来合并你分别跑出来的.gff文件；当然你可以用glob函数来代替这里的for循环！
use strict;
my $outputgff=shift;#输出总的.gff
my $path="/share/work/user121/project/stz/02.Annotation/01.repeatsequence/03.RepeatProteinMask/02.RepeatProteinMask.ABblast/divideintoscaffold/scaffold100";#使用时讲此处修改为gff文件所在的路径
my $num=2500;#此数值修改为你实际含有的gff文件的个数
open (O,">$outputgff");
for(my $i=1;$i<=$num;$i++){
    my $filepath=$path."/$i/$i.fa.gff";
    open(F,"$filepath");
    while(<F>){
        chomp;
        print O "$_\n";
    }
    close F;
}
close O;
