#!/usr/bin/perl -w
#此脚本用来输出运行trans.repeatproteinmasker.out.to.gff.pl 的sh文件
use strict;
my $envfile="export PERL5LIB=\$PERL5LIB:/share/data/software/cpan/share/perl/5.20.2:/share/data/software/cpan/lib/perl5";#加载环境变量
my $scriptpath="/share/work/user121/software_self/repeatmasker/tools/trans.repeatproteinmasker.out.to.gff.pl";#脚本路径
my $path="share/work/user121/project/stz/02.Annotation/01.repeatsequence/03.RepeatProteinMask/02.RepeatProteinMask.ABblast/divideintoscaffold/scaffold100";
my $filenum=2500;#可修改为文件个数
open(O,">$0.sh");
for(my $i=1;$i<=$filenum;$i++){
    my $scaffoldpath=$path."\/".$i."\/".$i.".fa";
    my $infilepath=$path."\/".$i."\/".$i.".fa.annot";
    my $outfilepath=$path."\/".$i."\/".$i.".fa.gff";
    print O "$envfile;$scriptpath $scaffoldpath $infilepath $outfilepath\n";
}
