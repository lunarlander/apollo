#!/usr/bin/perl -w
#按照gff文件将基因组中的重复序列1：碱基换成小写 2：碱基替换成n
use strict;
use Bio::SeqIO;
my ($genefile,$gffile,$outfile1,$outfile2)=@ARGV;#基因组文件,gff文件,输出mask:lc，输出mask:n
my %scaffold1;
my %scaffold2;
my @geneID;
my $fa=Bio::SeqIO->new(-file=>$genefile,-format=>'fasta');
while(my $seq=$fa->next_seq){
    my $id=$seq->id;
    my $seq=$seq->seq;
    push(@geneID,$id);
    $scaffold1{$id}=$seq;
    $scaffold2{$id}=$seq;
}
my @scaffoldID=keys %scaffold1;
open(F,"$gffile");
while(my $line=<F>){
    chomp $line;
    my @element=split(/\t/,$line);
    my $scaffoldname=@element[0];
    my $start=@element[3];my $end=@element[4];
    my $length=$end-$start+1;
    if(exists $scaffold1{$scaffoldname}){
	substr($scaffold1{$scaffoldname},$start,$length)=lc(substr($scaffold1{$scaffoldname},$start,$length));
    }
    if(exists $scaffold2{$scaffoldname}){
        substr($scaffold2{$scaffoldname},$start,$length)="n" x $length;
    }
}
close F;

open(O1,">$outfile1");
open(O2,">$outfile2");
foreach my $geneID(@geneID){
print O1 ">$geneID\n$scaffold1{$geneID}\n";
print O2 ">$geneID\n$scaffold2{$geneID}\n";
}
close O1;
close O2;
