#!/usr/bin/perl -w
#此脚本用来合并亚家族的信息，以及其占整个基因组的百分比，其中基因组大小用dict文件来计算
use strict;
my($genomedict,$output)=@ARGV;#分别输入基因组dict文件和输入文件名
die "Usage:$0 <genome size> <out put>\n" if(@ARGV!=2);
my $path="/home/user105/project/stz/02.Annotation/01.repeatsequence/00.result/02.STZ.repeatproteinmasker.abblast.gff/divide.gff/divide.gff.file";#将此修改为你存放bigfamily文件的路径！
my %hash;#定义一个存储家族为键和家族键值的哈希

print"Loading Genomesize...\n";
my $size=0;#统计基因组大小
open(F,"$genomedict");
while(my $line=<F>){
    chomp $line;
    if($line=~/(LN\:)([0-9]+)/){
        $size=$size+$2;
    }
}
close F;

print"Writing every file hash...";
my $filenum=500;#请修改为文件的个数
my $ID=0;
for(my $i=1;$i<=$filenum;$i++){
    my $gid="group".$i;
    my $file=$path."/$gid/$i.subfamily";
    open(F1,"$file")||die("$file\n");
    <F1>;
    <F1>;
    <F1>;
    while(my $line=<F1>){
        chomp $line;
        my @array=split(/\t/,$line);
        my $familyname=$array[0];
        my $sizeNUM=$array[1];
        $hash{$familyname}{$ID}=$sizeNUM;
        $ID++;
    }
    close F1;
}

open(O,">$output");
print O "subFamily\tsize\t%of genome\n";
foreach my $familyname(sort keys %hash){
    my $familylength=0;
    foreach my $ID(keys %{$hash{$familyname}}){
        $familylength+=$hash{$familyname}{$ID};
    }
    my $persent=$familylength/$size*100;
    print O "$familyname\t$familylength\t$persent\n";
}
close O;
