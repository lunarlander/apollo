1. 安装软件  
软件链接http://www.repeatmasker.org/RMDownload.html 使用wget命令获取  
注意：1、软件是在集成在repeatmasker软件里面，如果安装好repeatmasker，就无需再次安装。  
2、软件需要更新Libraries，site： http://www.girinst.org 需要学校***@stu.scu.edu.cn邮箱注册  
2. 将scaffold文件进行拆分  
脚本地址：apollo/0.genomeAnalysis/RepeatProteinMasker/01.trans.to.scaffold100.pl  
2.1 写.sh文件脚本批量跑repeatproteinmasker  
运行repeatproteinmasker命令：RepeatProteinMask -noLowSimple -pvalue 0.0001 -engine abblast[rmblast] ***.fa  
脚本：apollo/0.genomeAnalysis/RepeatProteinMasker/02.repeatproteinmask.abblast.sh.pl  
2.2 生成所有.pbs文件；提交所有任务  
2.3 检查所提交任务是否出错  
脚本：apollo/0.genomeAnalysis/RepeatProteinMasker/03.find.err.pl  
如果脚本运行完后没有提示信息，表明所提交任务运行过程中没有出现问题  
3. 将输出文件装换成gff格式  
脚本：apollo/0.genomeAnalysis/RepeatProteinMasker/04.trans.repeatproteinmasker.out.to.gff.pl  
！！！此脚本考虑到了.out文件中scaffold的名字一列输出限制为18个字节  
3.1 批量运行所有的.out 文件装换为.gff文件  
脚本：apollo/0.genomeAnalysis/RepeatProteinMasker/05.trans.all.gff.sh.pl  
3.2 合并所有的.gff文件，并修改.gff文件中的ID号  
脚本：apollo/0.genomeAnalysis/RepeatProteinMasker/06.combine.gff.pl（合并脚本）  
脚本：pollo/0.genomeAnalysis/RepeatProteinMasker/07.changeID.pl（修改ID号脚本）  
4. 统计.gff文件信息  
4.1 统计.gff文件中的重复序列的含量和运行08.repeatsize.pl脚本  
脚本：apollo/0.genomeAnalysis/RepeatProteinMasker/08.repeatsize.pl  
脚本：apollo/0.genomeAnalysis/RepeatProteinMasker/08.run.repeatsize.sh.pl  
4.2 按照gff文件将基因组中的重复序列1：碱基换成小写 2：碱基替换成n  
脚本：apollo/0.genomeAnalysis/RepeatProteinMasker/09.mask.pl  
4.3 统计大家族信息  
脚本：apollo/0.genomeAnalysis/RepeatProteinMasker/10.diff.bigfamily.pl  
4.4 统计亚家族信息  
脚本：apollo/0.genomeAnalysis/RepeatProteinMasker/11.diff.subfamily.pl  
4.3.1 按照scaffold名将gff文件进行分组  
脚本1：apollo/0.genomeAnalysis/RepeatProteinMasker/12.dividescaffoldname.pl  
脚本2：apollo/0.genomeAnalysis/RepeatProteinMasker/13.divide.gff.pl  
脚本1与脚本2配套使用，脚本1产生一个按照scaffoldname进行分组的list, 脚本2按照脚本1产生的list生成.gff文件  
4.3.1.1 批量跑统计大家族和亚家族脚本  
脚本1：apollo/0.genomeAnalysis/RepeatProteinMasker/14.run.bigfamily.sh.pl（大家族）  
脚本2：apollo/0.genomeAnalysis/RepeatProteinMasker/15.run.subfamily.sh.pl（亚家族）  
4.3.1.2整合大家族和亚家族脚本  
脚本1：apollo/0.genomeAnalysis/RepeatProteinMasker/16.combine.bigfamily.pl（大家族）  
脚本2：apollo/0.genomeAnalysis/RepeatProteinMasker/17.combine.subfamily.pl（亚家族）  