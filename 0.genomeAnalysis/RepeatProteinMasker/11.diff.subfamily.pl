#!/usr/bin/perl -w
#此脚本用来统计每个文件中亚家族的信息，输出每个亚家族名称，大小，占基因组大小
use strict;
my ($outfile,$genomedict,@file)=@ARGV;#输入输出文件名，基因组dict文件，不同软件跑出来的repeatproteinmasker结果

print"Loading genomesize.....\n";
my $genomesize=0;
open(F1,"$genomedict");
while(my $line=<F1>){
    chomp $line;
    if($line=~/(LN\:)([0-9]+)/){
        $genomesize=$genomesize+$2;
    }
}
close F1;

open(O,">$outfile");
foreach my $filename(@file){
    print O "==>**$filename**<==\n--------------------------------\nfamily\tsize\t%of genome\n";
    my %gffline;#定义一个键值为每一行内容的哈希
    print"loading $filename hash...\n";
    open(F,"$filename");
    while(my $line=<F>){
	chomp $line;
	my @element=split(/\t/,$line);#将每一行写入一个数组
	my $scaffoldname=$element[0];
	my $start=$element[3];my $end=$element[4];
	my $describe=$element[8];
	my @desarray=split(/;/,$element[8]);
	my $class=$desarray[2];
	my @family=split(/=/,$class);
	my $familyname=$family[1];
	my @bigfamily=split(/\//,$familyname);
	for($start;$start<=$end;$start++){
            $gffline{$familyname}{$scaffoldname}{$start}++;
        }
    }
    close F;

    print"stastic $filename...\n";
    my $percent=0;
    my @key1=keys %gffline;#class/family 的键
    foreach my $key1(sort @key1){
	my @key2=keys %{$gffline{$key1}};
	my $lengthnum=0;
	my $percent=0;
	foreach my $key2(@key2){
	    my $templength=0;
	    my @key3=keys %{$gffline{$key1}{$key2}};
	    #$templength=scalar(@key3);
	    foreach my $key3(@key3){
		$templength++;
	    }
	    $lengthnum=$lengthnum+$templength;
	    $percent=$lengthnum/$genomesize*100;
	}
	print O "$key1\t$lengthnum\t$percent\n";
    }
}
close O;


