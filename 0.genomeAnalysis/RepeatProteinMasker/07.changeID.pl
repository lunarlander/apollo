#!/usr/bin/perl -w
#此脚本的功能是修改合并好的.gff文件中的ID，使得ID号是从1开始有顺序的编写下去
use strict;
my $gfffile=shift;#读入的.gff文件
my $output=shift;#修改后的.gff文件
open(F,"$gfffile");
my $j=0;
my $l=8;#设置你想要的ID号的长度
open(O,">$output");
while(<F>){
    chomp;
    my @ARRAY=split(/\t/,$_);
    my @array=split(/;/,$ARRAY[8]);
    $j++;
    my $n=length $j;
    my $zero=$l-$n;
    my $ID="ID=TP".("0" x $zero).$j;
    $array[0]=$ID;
    my $new=join ";",@array;
    $ARRAY[8]=$new;
    my $newline=join("\t",@ARRAY);
    print O "$newline\n";
}
close F;
close O;
