#!/usr/bin/perl -w
#此脚本用来将trf,repeatmasker,repeatproteinmasker软件跑出来的gff文件进行分组
use strict;
use List::Util;
use FileHandle;
my ($scaffoldgroup,$tatolgff)=@ARGV;#打开scaffold分组文件,此文件是由，gff文件
die "Usage: $0 <scaffoldgroup> <gff file>\n" if(@ARGV!=2);

my $path="/home/user105/project/stz/02.Annotation/01.repeatsequence/00.result/total.gff/total.gff.divide/01.total.gff.divide";#可修改为你想创建gff文件的路劲
my %fh;#创建一个哈希来保存文件句柄
my %hash;
my $mark=10000;#定义一个程序运行mark，其数值可以任意修改。其中10000表示的是当程序循环运行10000次就有一次输出！

my $groupnum=500;#修改为你产生了多少group
for(my $i=1;$i<=$groupnum;$i++){
    my $gid="group".$i;
    `cd $path;mkdir $gid`;#调用系统命令，先进入到你想创建gff文件的目录下面，然后再调用系统命令创建文件夹。
    my $filepath=$path."/$gid/$gid";
    open($fh{$gid},">$filepath");
}

open(F1,"$scaffoldgroup");
open(F2,"$tatolgff");
my $num=0;
while(my $line1=<F1>){
    chomp $line1;
    my @array=split(/\t/,$line1);
    $hash{$array[0]}=$array[1];
}

while(my $line2=<F2>){
    chomp $line2;
    my @ARRAY=split(/\t/,$line2);
    $fh{$hash{$ARRAY[0]}}-> print ("$line2\n");
    $num++;
    if($num%$mark==0){print"$num\n";}
}

foreach my $O(keys %fh){
    close $fh{$O};
}
