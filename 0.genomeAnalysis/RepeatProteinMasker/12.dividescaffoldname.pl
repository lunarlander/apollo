#!/usr/bin/perl -w
#按照scaffold的名称进行分组，500个名为一组，此脚本与后面的按照scaffold名分gff脚本配套使用
use strict;
use List::Util;
my ($scaffoldict,$listfile)=@ARGV;
die"Usage: $0 <scaffold dict> <list file>\n" if(@ARGV!=2);
my @scaffoldname;
my $groupnum=500;#此数值可修改为你所想的分组个数

open(F,"$scaffoldict");
<F>;
while(my $line=<F>){
    chomp $line;
    my @array1=split(/\t/,$line);
    my @array2=split(/:/,$array1[1]);
    push(@scaffoldname,$array2[1]);
}
close F;

@scaffoldname=List::Util::shuffle @scaffoldname;
my $length=scalar(@scaffoldname);
open(O,">$listfile");
my $groplength=int($length/$groupnum)+1;
my $j=0;
for(my $i=0;$i<$length;$i++){
    if($i%$groplength==0){$j++;}
    print O "$scaffoldname[$i]\tgroup$j\n";
}
close O;
