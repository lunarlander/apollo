#!/usr/bin/perl -w
#此脚本用来书写统计.gff文件中的重复序列的含量.sh文件：repeatsize.pl
use strict;
$num=500;#文件的个数
my $scriptpath="apollo/0.genomeAnalysis/RepeatProteinMasker/08.repeatsize.pl";#脚本路径
my $path="/home/user105/project/stz/02.Annotation/01.repeatsequence/00.result/total.gff/total.gff.divide/01.total.gff.divide";#输入输出文件放在一个路径下
my $genomedict="/home/user105/project/stz/02.Annotation/01.repeatsequence/00.STZ.scaffold/STZ.scaffold.fa.dict";#基因组dict文件
open(O,">$0.sh");
for(my $i=1;$i<=$num;$i++){
    my $gid="group".$i;
    my $outpath=$path."\/".$gid."\/".$i.".rep.stas";
    my $inpath=$path."\/".$gid."\/".$gid;
    print O "$scriptpath $outpath $genomedict $inpath\n";
}
close O;