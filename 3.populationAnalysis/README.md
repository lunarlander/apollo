# What is this pipeline series for?  #

本流程包括的内容

- reads比对、过滤和re-align
- SNP鉴定与过滤
- 一致性序列的鉴定
- phase分形
- 频谱的生成
- Pi、FST、tajimaD、Dxy统计
- ld、重组率的计算
- 系统发育树、PCA、structure分析
- 选择区域的鉴定
- 关联分析
- 分化岛屿的鉴定
- abba分析
- f3 f4检验
- 群体历史模拟