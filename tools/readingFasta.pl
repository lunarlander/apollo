#! /usr/bin/perl
use strict;
use warnings;
use Bio::SeqIO;

my $file=shift;

my $fa=Bio::SeqIO->new(-file=>$file,-format=>'fasta');

while(my $seq=$fa->next_seq){
    my $id=$seq->id;
    my $seq=$seq->seq;
    print ">$id\n$seq\n";
}
